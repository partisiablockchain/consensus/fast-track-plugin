package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/** Test of {@link FastTrackLocal}. */
public final class FastTrackLocalTest {

  @Test
  public void increase() {
    FastTrackLocal local = new FastTrackLocal();
    assertThat(local.getResetBlocksSeen()).isEqualTo(0);

    local = local.increaseBlocksSeen(1);
    assertThat(local.getResetBlocksSeen()).isEqualTo(1);

    local = local.increaseBlocksSeen(1);
    assertThat(local.getResetBlocksSeen()).isEqualTo(2);

    local = local.increaseBlocksSeen(7);
    assertThat(local.getResetBlocksSeen()).isEqualTo(3);
  }

  @Test
  public void increaseCommittees() {
    FastTrackLocal local = new FastTrackLocal();
    assertThat(local.getActiveCommittee()).isEqualTo(0);

    local = local.increaseBlocksSeen(1);
    assertThat(local.getActiveCommittee()).isEqualTo(1);

    local = local.increaseBlocksSeen(2);
    assertThat(local.getActiveCommittee()).isEqualTo(2);

    local = local.increaseBlocksSeen(2);
    assertThat(local.getActiveCommittee()).isEqualTo(2);
  }
}
