package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.consensus.fasttrack.FastTrackGlobal.getSet;

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin.BlockValidation;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FastTrackGlobalTest {

  private static final Hash emptyHash = Hash.create(FunctionUtility.noOpConsumer());

  private FastTrackGlobal initial;
  private FastTrackGlobal withSevenProducers;
  private Block block;

  /** Setup plugin. */
  @BeforeEach
  public void setUp() {
    initial = createInitial();
    new FastTrackGlobal();
    withSevenProducers =
        FastTrackGlobal.initialize(
            Hash.create(stream -> stream.writeInt(4418)),
            List.of(
                Accounts.ONE_MEMBER,
                Accounts.TWO_MEMBER,
                Accounts.THREE_MEMBER,
                Accounts.FOUR_MEMBER,
                Accounts.FIVE_MEMBER,
                Accounts.SIX_MEMBER,
                Accounts.SEVEN_MEMBER));
    block = createBlock();
  }

  private static Block createBlock() {
    return createBlock(0);
  }

  private static Block createBlock(int producerIndex) {
    return new Block(
        System.currentTimeMillis(),
        10,
        0,
        emptyHash,
        emptyHash,
        List.of(),
        List.of(),
        producerIndex);
  }

  static FastTrackGlobal createInitial() {
    return FastTrackGlobal.initialize(
        Hash.create(stream -> stream.writeInt(0)),
        List.of(
            Accounts.ONE_MEMBER, Accounts.TWO_MEMBER, Accounts.THREE_MEMBER, Accounts.FOUR_MEMBER));
  }

  @Test
  public void committeesNewNonce() {
    List<CommitteeMember> producers =
        List.of(
            Accounts.ONE_MEMBER, Accounts.TWO_MEMBER, Accounts.THREE_MEMBER, Accounts.FOUR_MEMBER);
    FastTrackGlobal withFour = FastTrackGlobal.initialize(emptyHash, producers);
    List<CommitteeMember> producers2 =
        List.of(
            Accounts.FIVE_MEMBER,
            Accounts.SIX_MEMBER,
            Accounts.SEVEN_MEMBER,
            Accounts.EIGHT_MEMBER);
    withFour = withFour.setCommittee(emptyHash, FixedList.create(producers2));
    int committees = withFour.getNewestCommitteeId();
    Assertions.assertThat(committees).isEqualTo(1);
  }

  @Test
  public void withSevenProducers() {
    List<CommitteeMember> producers =
        List.of(
            Accounts.ONE_MEMBER,
            Accounts.TWO_MEMBER,
            Accounts.THREE_MEMBER,
            Accounts.FOUR_MEMBER,
            Accounts.FIVE_MEMBER,
            Accounts.SIX_MEMBER,
            Accounts.SEVEN_MEMBER);
    FastTrackGlobal withSeven = FastTrackGlobal.initialize(emptyHash, producers);
    List<BlockchainPublicKey> keys =
        List.of(
            Accounts.ONE,
            Accounts.TWO,
            Accounts.THREE,
            Accounts.FOUR,
            Accounts.FIVE,
            Accounts.SIX,
            Accounts.SEVEN);
    Assertions.assertThat(withSeven.getCommitteeKeys(0)).containsExactlyInAnyOrderElementsOf(keys);
  }

  @Test
  public void initializeTooFewProducers() {
    Assertions.assertThatThrownBy(
            () ->
                FastTrackGlobal.initialize(
                    emptyHash,
                    List.of(Accounts.ONE_MEMBER, Accounts.TWO_MEMBER, Accounts.THREE_MEMBER)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Illegal committee size. Must be of size 3t+1");
    Assertions.assertThatThrownBy(
            () -> FastTrackGlobal.initialize(emptyHash, List.of(Accounts.ONE_MEMBER)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Illegal committee size. Must be of size 3t+1");
    Assertions.assertThatThrownBy(() -> FastTrackGlobal.initialize(emptyHash, List.of()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Illegal committee size. Must be of size 3t+1");
  }

  @Test
  public void initializeDuplicateProducer() {
    Assertions.assertThatThrownBy(
            () ->
                FastTrackGlobal.initialize(
                    emptyHash,
                    List.of(
                        Accounts.ONE_MEMBER,
                        Accounts.TWO_MEMBER,
                        Accounts.THREE_MEMBER,
                        new CommitteeMember(
                            Accounts.EIGHT_MEMBER.getIdentity(), Accounts.ONE, Accounts.BLS_ONE))))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Producers contain duplicate addresses");
    Assertions.assertThatThrownBy(
            () ->
                FastTrackGlobal.initialize(
                    emptyHash,
                    List.of(
                        Accounts.ONE_MEMBER,
                        Accounts.TWO_MEMBER,
                        Accounts.THREE_MEMBER,
                        new CommitteeMember(
                            Accounts.ONE_MEMBER.getIdentity(),
                            Accounts.EIGHT,
                            Accounts.BLS_EIGHT))))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Producers contain duplicate addresses");
  }

  @Test
  public void notInitialized() {
    FastTrackGlobal fastTrack = new FastTrackGlobal();
    Assertions.assertThatThrownBy(
            () ->
                fastTrack.validateFinalizedBlock(
                    new FinalBlock(
                        block,
                        signaturesValidProducer(
                            new byte[] {0b0111},
                            Accounts.BLS_KEY_TWO,
                            Accounts.BLS_KEY_THREE,
                            Accounts.BLS_KEY_FOUR))))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("FastTrackGlobal not initialized");
  }

  @Test
  public void blockWithTooHighCommitteeId() {
    Block invalidBlock =
        new Block(
            System.currentTimeMillis(), 10, 2, emptyHash, emptyHash, List.of(), List.of(), -1);
    BlockValidation blockValidation =
        initial.validateFinalizedBlock(
            new FinalBlock(
                invalidBlock,
                signaturesValidProducer(
                    new byte[] {0b1110},
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(blockValidation.isAccepted()).isFalse();
    Block invalidBlock2 =
        new Block(
            System.currentTimeMillis(), 10, 1, emptyHash, emptyHash, List.of(), List.of(), -1);
    blockValidation =
        initial.validateFinalizedBlock(
            new FinalBlock(
                invalidBlock2,
                signaturesValidProducer(
                    new byte[] {0b1110},
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(blockValidation.isAccepted()).isFalse();
  }

  @Test
  public void verifySerialization() {
    StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
    SerializationResult result = serializer.write(initial);
    FastTrackGlobal read = serializer.read(result.hash(), FastTrackGlobal.class);
    Assertions.assertThat(read.getCommitteeId(0)).isEqualTo(initial.getCommitteeId(0));
  }

  @Test
  public void getCommittee() {
    Assertions.assertThat(initial.getCommitteeKeys(0))
        .hasSize(4)
        .containsExactlyInAnyOrder(Accounts.ONE, Accounts.TWO, Accounts.THREE, Accounts.FOUR);
    Assertions.assertThat(withSevenProducers.getCommitteeKeys(0))
        .hasSize(7)
        .containsExactly(
            Accounts.ONE,
            Accounts.TWO,
            Accounts.THREE,
            Accounts.FOUR,
            Accounts.FIVE,
            Accounts.SIX,
            Accounts.SEVEN);
    Assertions.assertThat(withSevenProducers.getCommitteeBlsKeys(0))
        .hasSize(7)
        .containsExactly(
            Accounts.BLS_ONE,
            Accounts.BLS_TWO,
            Accounts.BLS_THREE,
            Accounts.BLS_FOUR,
            Accounts.BLS_FIVE,
            Accounts.BLS_SIX,
            Accounts.BLS_SEVEN);
  }

  @Test
  public void validateFinalBlockValid() {
    BlockValidation validation = initial.validateFinalizedBlock(validForInitial(0));
    Assertions.assertThat(validation.isAccepted()).isTrue();
    Assertions.assertThat(validation.canRollback()).isTrue();

    BlockValidation secondValidation =
        initial.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b1011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(secondValidation.isAccepted()).isTrue();

    BlockValidation largeValidation =
        withSevenProducers.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b1111011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_SEVEN,
                    Accounts.BLS_KEY_SIX,
                    Accounts.BLS_KEY_FIVE)));
    Assertions.assertThat(largeValidation.isAccepted()).isTrue();
  }

  @Test
  public void validateFinalBlockAdditionalFinalizationData() {
    BlockValidation actual =
        initial.validateFinalizedBlock(
            new FinalBlock(
                block,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.write(
                          signaturesValidProducer(
                              new byte[] {0b1010}, Accounts.BLS_KEY_TWO, Accounts.BLS_KEY_FOUR));
                      stream.writeByte(1);
                    })));
    Assertions.assertThat(actual.isAccepted()).isFalse();
  }

  @Test
  public void validateFinalBlockInvalidSigner() {
    BlockValidation actual =
        initial.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    // there's only 4 accounts in initial, so the bit vector has to reflect that.
                    new byte[] {0b1011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_FIVE)));
    Assertions.assertThat(actual.isAccepted()).isFalse();
  }

  @Test
  public void validateFinalBlockTooFewSignature() {
    BlockValidation noSignatures =
        initial.validateFinalizedBlock(
            new FinalBlock(block, signaturesValidProducer(new byte[1], Accounts.BLS_KEY_ONE)));
    Assertions.assertThat(noSignatures.isAccepted()).isFalse();

    BlockValidation actual =
        initial.validateFinalizedBlock(
            new FinalBlock(
                block, signaturesValidProducer(new byte[] {0b1000}, Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(actual.isAccepted()).isFalse();

    BlockValidation largeValidation =
        withSevenProducers.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b101100},
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_SIX,
                    Accounts.BLS_KEY_THREE)));
    Assertions.assertThat(largeValidation.isAccepted()).isFalse();
  }

  @Test
  public void getSetTest() {
    int b = 0b11001_1010101;
    List<Integer> set = getSet(new byte[] {(byte) (b & 0xFF), (byte) ((b & 0xFF00) >> 8)}, 16);
    Assertions.assertThat(set).containsExactly(0, 2, 4, 6, 7, 10, 11);
    List<Integer> setTruncated =
        getSet(new byte[] {(byte) (b & 0xFF), (byte) ((b & 0xFF00) >> 8)}, 11);
    Assertions.assertThat(setTruncated).containsExactly(0, 2, 4, 6, 7, 10);
    List<Integer> setSmall = getSet(new byte[] {0b1}, 1);
    Assertions.assertThat(setSmall).containsExactly(0);
  }

  @Test
  public void validResetBlock() {
    block = createBlock(-1);
    BlockValidation blockValidation =
        initial.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b1011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR)));

    Assertions.assertThat(blockValidation.isAccepted()).isTrue();
    Assertions.assertThat(blockValidation.canRollback()).isFalse();
  }

  @Test
  public void validateFinalBlockForSixProducers() {
    FastTrackGlobal initialSix =
        FastTrackGlobal.initialize(
            Hash.create(stream -> stream.writeInt(0)),
            List.of(
                Accounts.ONE_MEMBER,
                Accounts.TWO_MEMBER,
                Accounts.THREE_MEMBER,
                Accounts.FOUR_MEMBER,
                Accounts.FIVE_MEMBER,
                Accounts.SIX_MEMBER));

    // Too few
    BlockValidation largeValidationTooFew =
        initialSix.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b101100},
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_SIX,
                    Accounts.BLS_KEY_THREE)));
    Assertions.assertThat(largeValidationTooFew.isAccepted()).isFalse();

    // Enough
    BlockValidation largeValidation =
        initialSix.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b101111},
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_SIX,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_ONE)));
    Assertions.assertThat(largeValidation.isAccepted()).isTrue();
  }

  @Test
  public void validateFinalBlockForFiveProducers() {
    FastTrackGlobal initialFive =
        FastTrackGlobal.initialize(
            Hash.create(stream -> stream.writeInt(0)),
            List.of(
                Accounts.ONE_MEMBER,
                Accounts.TWO_MEMBER,
                Accounts.THREE_MEMBER,
                Accounts.FOUR_MEMBER,
                Accounts.FIVE_MEMBER));

    // Too few
    BlockValidation largeValidationTooFew =
        initialFive.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b11100},
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_FIVE,
                    Accounts.BLS_KEY_THREE)));
    Assertions.assertThat(largeValidationTooFew.isAccepted()).isFalse();

    // Enough
    BlockValidation largeValidation =
        initialFive.validateFinalizedBlock(
            new FinalBlock(
                block,
                signaturesValidProducer(
                    new byte[] {0b11110},
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_FIVE,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_TWO)));
    Assertions.assertThat(largeValidation.isAccepted()).isTrue();
  }

  @Test
  public void resetBlockTooFewSignature() {
    BlockValidation blockValidation =
        initial.validateFinalizedBlock(
            new FinalBlock(
                createBlock(-1),
                signatures(
                    block, new byte[] {0b1001}, Accounts.BLS_KEY_ONE, Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(blockValidation.isAccepted()).isFalse();
  }

  private byte[] signaturesValidProducer(byte[] bitVector, BlsKeyPair... signers) {
    return signatures(block, bitVector, signers);
  }

  static FinalBlock validForInitial(int producerIndex) {
    Block block = createBlock(producerIndex);
    return new FinalBlock(
        block,
        signatures(
            block,
            new byte[] {0b1111},
            Accounts.BLS_KEY_ONE,
            Accounts.BLS_KEY_TWO,
            Accounts.BLS_KEY_THREE,
            Accounts.BLS_KEY_FOUR));
  }

  /**
   * Returns a byte array of signatures on a block.
   *
   * @param block block to be signed
   * @param signers signers of the block
   * @return byte array of signatures
   */
  public static byte[] signatures(Block block, byte[] bitVector, BlsKeyPair... signers) {
    Hash identifier = block.identifier();

    List<BlsSignature> signatures =
        Arrays.stream(signers).map(signer -> signer.sign(identifier)).collect(Collectors.toList());

    BlsSignature sig = null;
    for (BlsSignature s : signatures) {
      if (sig == null) {
        sig = s;
      } else {
        sig = sig.addSignature(s);
      }
    }

    final BlsSignature finalSig = sig;
    byte[] serialize =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeDynamicBytes(bitVector);
              finalSig.write(s);
            });
    return serialize;
  }
}
