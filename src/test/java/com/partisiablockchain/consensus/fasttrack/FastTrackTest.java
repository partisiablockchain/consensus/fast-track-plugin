package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin.BlockValidation;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FastTrackTest {

  private final FastTrack plugin = new FastTrack();

  private final Hash emptyHash = Hash.create(FunctionUtility.noOpConsumer());

  @Test
  public void validateInvalidFinalizedBlockGlobal() {
    Block block =
        new Block(
            System.currentTimeMillis(),
            10,
            0,
            Hash.create(FunctionUtility.noOpConsumer()),
            Hash.create(FunctionUtility.noOpConsumer()),
            List.of(),
            List.of(),
            0);
    boolean validation =
        plugin.validateExternalBlock(
            FastTrackGlobalTest.createInitial(),
            new FinalBlock(
                block,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b11011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_FIVE)));
    Assertions.assertThat(validation).isFalse();
  }

  @Test
  public void validateFinalizedBlockLocal() {
    Block block =
        new Block(
            System.currentTimeMillis(),
            10,
            0,
            Hash.create(FunctionUtility.noOpConsumer()),
            Hash.create(FunctionUtility.noOpConsumer()),
            List.of(),
            List.of(),
            0);
    BlockValidation validation =
        plugin.validateLocalBlock(
            FastTrackGlobalTest.createInitial(),
            new FastTrackLocal(0, 0),
            new FinalBlock(
                block,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b11011},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_FOUR,
                    Accounts.BLS_KEY_FIVE)));
    Assertions.assertThat(validation.isAccepted()).isFalse();
  }

  @Test
  public void validateFinalizedBlock() {
    Block block2 =
        new Block(System.currentTimeMillis(), 10, 0, emptyHash, emptyHash, List.of(), List.of(), 0);
    BlockValidation validationFail23 =
        plugin.validateLocalBlock(
            FastTrackGlobalTest.createInitial(),
            new FastTrackLocal(0, 0),
            new FinalBlock(
                block2,
                FastTrackGlobalTest.signatures(
                    block2,
                    new byte[] {0b1111},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(validationFail23.isAccepted()).isTrue();
    Block block =
        new Block(
            System.currentTimeMillis(), 10, 0, emptyHash, emptyHash, List.of(), List.of(), -1);
    Block illegalBlock =
        new Block(System.currentTimeMillis(), 10, 0, emptyHash, emptyHash, List.of(), List.of(), 0);
    FastTrackGlobal updated =
        FastTrackGlobalTest.createInitial()
            .setCommittee(
                Hash.create(stream -> stream.writeInt(1)),
                FixedList.create(
                    List.of(
                        Accounts.FIVE_MEMBER,
                        Accounts.SIX_MEMBER,
                        Accounts.SEVEN_MEMBER,
                        Accounts.EIGHT_MEMBER)));
    BlockValidation validation =
        plugin.validateLocalBlock(
            updated,
            new FastTrackLocal(0, 0),
            new FinalBlock(
                block,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b1111},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(validation.isAccepted()).isTrue();
    BlockValidation validationFail =
        plugin.validateLocalBlock(
            updated,
            new FastTrackLocal(0, 0),
            new FinalBlock(
                illegalBlock,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b1111},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(validationFail.isAccepted()).isFalse();
    illegalBlock =
        new Block(System.currentTimeMillis(), 10, 1, emptyHash, emptyHash, List.of(), List.of(), 0);
    BlockValidation validationFail2 =
        plugin.validateLocalBlock(
            updated,
            new FastTrackLocal(0, 1),
            new FinalBlock(
                illegalBlock,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b1111},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(validationFail2.isAccepted()).isFalse();
    Block illegalBlock2 =
        new Block(System.currentTimeMillis(), 10, 2, emptyHash, emptyHash, List.of(), List.of(), 0);
    BlockValidation blockValidation =
        plugin.validateLocalBlock(
            updated,
            new FastTrackLocal(0, 2),
            new FinalBlock(
                illegalBlock2,
                FastTrackGlobalTest.signatures(
                    block,
                    new byte[] {0b1111},
                    Accounts.BLS_KEY_ONE,
                    Accounts.BLS_KEY_TWO,
                    Accounts.BLS_KEY_THREE,
                    Accounts.BLS_KEY_FOUR)));
    Assertions.assertThat(blockValidation.isAccepted()).isFalse();
  }

  @Test
  public void validateFinalizedBlockValid() {
    BlockValidation validation =
        plugin.validateLocalBlock(
            FastTrackGlobalTest.createInitial(),
            new FastTrackLocal(0, 0),
            FastTrackGlobalTest.validForInitial(-1));
    Assertions.assertThat(validation.isAccepted()).isTrue();
  }

  @Test
  public void invalidCommitteeId() {
    BlockValidation validation =
        plugin.validateLocalBlock(
            FastTrackGlobalTest.createInitial(),
            new FastTrackLocal(0, 3),
            FastTrackGlobalTest.validForInitial(0));
    Assertions.assertThat(validation.isAccepted()).isFalse();
  }

  @Test
  public void validateValidFinalizedBlockGlobal() {
    boolean validation =
        plugin.validateExternalBlock(
            FastTrackGlobalTest.createInitial(), FastTrackGlobalTest.validForInitial(0));
    Assertions.assertThat(validation).isTrue();
  }

  @Test
  public void getGlobalStateClass() {
    Assertions.assertThat(plugin.getGlobalStateClass()).isEqualTo(FastTrackGlobal.class);
  }

  @Test
  public void invokeGlobal() {
    FastTrackGlobal state = FastTrackGlobalTest.createInitial();
    FastTrackGlobal updated =
        plugin
            .invokeGlobal(
                null,
                state,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeEnum(GlobalInvocation.SET_COMMITTEE);
                      state.getCommitteeId(0).write(stream);
                      CommitteeMember.LIST_STREAM.writeDynamic(
                          stream,
                          List.of(
                              Accounts.ONE_MEMBER,
                              Accounts.TWO_MEMBER,
                              Accounts.THREE_MEMBER,
                              Accounts.FIVE_MEMBER));
                    }))
            .updatedState();
    Assertions.assertThat(updated.getCommitteeKeys(1))
        .containsExactly(Accounts.ONE, Accounts.TWO, Accounts.THREE, Accounts.FIVE);
  }

  @Test
  public void migrateGlobal() {
    Hash committeeId = Hash.create(s -> s.writeInt(12345));
    FastTrackGlobal initial =
        plugin.migrateGlobal(
            null,
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(
                    stream -> {
                      CommitteeMember.LIST_STREAM.writeDynamic(
                          stream,
                          List.of(
                              Accounts.ONE_MEMBER,
                              Accounts.TWO_MEMBER,
                              Accounts.THREE_MEMBER,
                              Accounts.FOUR_MEMBER));
                      committeeId.write(stream);
                    })));
    Assertions.assertThat(initial.getCommitteeId(0)).isEqualTo(committeeId);
    Assertions.assertThat(initial.getCommitteeBlsKeys(0))
        .isEqualTo(
            List.of(Accounts.BLS_ONE, Accounts.BLS_TWO, Accounts.BLS_THREE, Accounts.BLS_FOUR));
    Hash committeeHash = Hash.create(s -> s.writeInt(123));
    List<CommitteeMember> committeesMembers =
        List.of(
            Accounts.ONE_MEMBER, Accounts.TWO_MEMBER, Accounts.THREE_MEMBER, Accounts.FIVE_MEMBER);
    FastTrackGlobal global = FastTrackGlobal.initialize(committeeHash, committeesMembers);
    int newestCommitteeId = global.getNewestCommitteeId();
    StateAccessor accessor = StateAccessor.create(global);
    FastTrackGlobal updatedGlobal =
        plugin.migrateGlobal(accessor, SafeDataInputStream.createFromBytes(new byte[0]));
    Assertions.assertThat(global).usingRecursiveComparison().isEqualTo(updatedGlobal);
    Assertions.assertThat(updatedGlobal.getCommitteeId(newestCommitteeId)).isEqualTo(committeeHash);
    Assertions.assertThat(updatedGlobal.getCommitteeBlsKeys(newestCommitteeId))
        .isEqualTo(
            List.of(Accounts.BLS_ONE, Accounts.BLS_TWO, Accounts.BLS_THREE, Accounts.BLS_FIVE));
  }

  @Test
  public void updateForBlock() {
    FastTrackGlobal state = FastTrackGlobalTest.createInitial();
    final Block normalBlock =
        new Block(0, 0, 0, Block.GENESIS_PARENT, Block.GENESIS_PARENT, List.of(), List.of(), 0);

    final Block resetBlock =
        new Block(0, 0, 0, Block.GENESIS_PARENT, Block.GENESIS_PARENT, List.of(), List.of(), -1);

    FastTrackLocal notIncreased =
        plugin.updateForBlock(null, state, new FastTrackLocal(), normalBlock);
    Assertions.assertThat(notIncreased.getResetBlocksSeen()).isEqualTo(0);

    FastTrackLocal increased = plugin.updateForBlock(null, state, new FastTrackLocal(), resetBlock);
    Assertions.assertThat(increased.getResetBlocksSeen()).isEqualTo(1);
  }

  @Test
  public void migrateLocal() {
    FastTrackLocal local = new FastTrackLocal();
    StateAccessor accessor = StateAccessor.create(local);
    FastTrackLocal updatedLocal = plugin.migrateLocal(accessor);
    FastTrackLocal localNull = plugin.migrateLocal(null);
    Assertions.assertThat(local.getResetBlocksSeen()).isEqualTo(updatedLocal.getResetBlocksSeen());
    Assertions.assertThat(local.getActiveCommittee()).isEqualTo(updatedLocal.getActiveCommittee());
    Assertions.assertThat(localNull).usingRecursiveComparison().isEqualTo(local);

    FastTrackLocal localWithValues = new FastTrackLocal(2, 3);
    StateAccessor accessorWithValues = StateAccessor.create(localWithValues);
    FastTrackLocal updatedLocalWithValues =
        FastTrackLocal.createFromStateAccessor(accessorWithValues);
    Assertions.assertThat(localWithValues.getResetBlocksSeen())
        .isEqualTo(updatedLocalWithValues.getResetBlocksSeen());
    Assertions.assertThat(localWithValues.getActiveCommittee())
        .isEqualTo(updatedLocalWithValues.getActiveCommittee());
  }

  @Test
  public void localStateClass() {
    Class<FastTrackLocal> localClass = plugin.getLocalStateClass();
    Assertions.assertThat(localClass).isEqualTo(FastTrackLocal.class);
  }
}
