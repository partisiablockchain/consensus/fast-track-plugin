package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;

final class Accounts {

  static final KeyPair KEY_ONE = new KeyPair(BigInteger.ONE);
  static final BlockchainPublicKey ONE = KEY_ONE.getPublic();

  static final BlsKeyPair BLS_KEY_ONE = new BlsKeyPair(BigInteger.valueOf(11));
  static final BlsPublicKey BLS_ONE = BLS_KEY_ONE.getPublicKey();
  static final CommitteeMember ONE_MEMBER =
      new CommitteeMember(KEY_ONE.getPublic().createAddress(), ONE, BLS_ONE);

  static final KeyPair KEY_TWO = new KeyPair(BigInteger.valueOf(2));
  static final BlockchainPublicKey TWO = KEY_TWO.getPublic();
  static final BlsKeyPair BLS_KEY_TWO = new BlsKeyPair(BigInteger.valueOf(12));
  static final BlsPublicKey BLS_TWO = BLS_KEY_TWO.getPublicKey();
  static final CommitteeMember TWO_MEMBER =
      new CommitteeMember(KEY_TWO.getPublic().createAddress(), TWO, BLS_TWO);

  static final KeyPair KEY_THREE = new KeyPair(BigInteger.valueOf(3));
  static final BlockchainPublicKey THREE = KEY_THREE.getPublic();
  static final BlsKeyPair BLS_KEY_THREE = new BlsKeyPair(BigInteger.valueOf(13));
  static final BlsPublicKey BLS_THREE = BLS_KEY_THREE.getPublicKey();
  static final CommitteeMember THREE_MEMBER =
      new CommitteeMember(KEY_THREE.getPublic().createAddress(), THREE, BLS_THREE);

  static final KeyPair KEY_FOUR = new KeyPair(BigInteger.valueOf(4));
  static final BlockchainPublicKey FOUR = KEY_FOUR.getPublic();
  static final BlsKeyPair BLS_KEY_FOUR = new BlsKeyPair(BigInteger.valueOf(14));
  static final BlsPublicKey BLS_FOUR = BLS_KEY_FOUR.getPublicKey();
  static final CommitteeMember FOUR_MEMBER =
      new CommitteeMember(KEY_FOUR.getPublic().createAddress(), FOUR, BLS_FOUR);

  static final KeyPair KEY_FIVE = new KeyPair(BigInteger.valueOf(5));
  static final BlockchainPublicKey FIVE = KEY_FIVE.getPublic();
  static final BlsKeyPair BLS_KEY_FIVE = new BlsKeyPair(BigInteger.valueOf(15));
  static final BlsPublicKey BLS_FIVE = BLS_KEY_FIVE.getPublicKey();
  static final CommitteeMember FIVE_MEMBER =
      new CommitteeMember(KEY_FIVE.getPublic().createAddress(), FIVE, BLS_FIVE);

  static final KeyPair KEY_SIX = new KeyPair(BigInteger.valueOf(6));
  static final BlockchainPublicKey SIX = KEY_SIX.getPublic();
  static final BlsKeyPair BLS_KEY_SIX = new BlsKeyPair(BigInteger.valueOf(16));
  static final BlsPublicKey BLS_SIX = BLS_KEY_SIX.getPublicKey();
  static final CommitteeMember SIX_MEMBER =
      new CommitteeMember(KEY_SIX.getPublic().createAddress(), SIX, BLS_SIX);

  static final KeyPair KEY_SEVEN = new KeyPair(BigInteger.valueOf(7));
  static final BlockchainPublicKey SEVEN = KEY_SEVEN.getPublic();
  static final BlsKeyPair BLS_KEY_SEVEN = new BlsKeyPair(BigInteger.valueOf(17));
  static final BlsPublicKey BLS_SEVEN = BLS_KEY_SEVEN.getPublicKey();
  static final CommitteeMember SEVEN_MEMBER =
      new CommitteeMember(KEY_SEVEN.getPublic().createAddress(), SEVEN, BLS_SEVEN);

  private static final KeyPair KEY_EIGHT = new KeyPair(BigInteger.valueOf(8));
  static final BlockchainPublicKey EIGHT = KEY_EIGHT.getPublic();
  static final BlsKeyPair BLS_KEY_EIGHT = new BlsKeyPair(BigInteger.valueOf(18));
  static final BlsPublicKey BLS_EIGHT = BLS_KEY_EIGHT.getPublicKey();
  static final CommitteeMember EIGHT_MEMBER =
      new CommitteeMember(KEY_EIGHT.getPublic().createAddress(), EIGHT, BLS_EIGHT);
}
