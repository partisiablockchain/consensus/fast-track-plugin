package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.tools.immutable.FixedList;

/** A committee in fast track. */
@Immutable
public final class Committee implements StateSerializableInline {

  private final Hash committeeId;
  private final FixedList<CommitteeMember> activeCommittee;

  @SuppressWarnings("unused")
  private Committee() {
    committeeId = null;
    activeCommittee = null;
  }

  /**
   * Default constructor.
   *
   * @param committeeId identifier for committee
   * @param activeCommittee list of members for committee
   */
  public Committee(Hash committeeId, FixedList<CommitteeMember> activeCommittee) {
    this.committeeId = committeeId;
    this.activeCommittee = activeCommittee;
  }

  static Committee createFromStateAccessor(StateAccessor accessor) {
    Hash committeeId = accessor.get("committeeId").hashValue();
    FixedList<CommitteeMember> activeCommittee =
        FixedList.create(
            accessor.get("activeCommittee").getListElements().stream()
                .map(CommitteeMember::createFromStateAccessor));
    return new Committee(committeeId, activeCommittee);
  }

  /**
   * Get identifier for committee.
   *
   * @return identifying hash
   */
  public Hash getCommitteeId() {
    return committeeId;
  }

  /**
   * Get list of active members for committee.
   *
   * @return list of {@link CommitteeMember}
   */
  public FixedList<CommitteeMember> getActiveCommittee() {
    return activeCommittee;
  }
}
