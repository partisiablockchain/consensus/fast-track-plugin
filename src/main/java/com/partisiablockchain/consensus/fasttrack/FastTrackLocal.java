package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Local state for the fast track plugin. */
@Immutable
public final class FastTrackLocal implements StateSerializable {

  private final int resetBlocksSeen;

  private final int activeCommittee;

  /** Construct an uninitialized local state. */
  @SuppressWarnings("unused")
  public FastTrackLocal() {
    this(0, 0);
  }

  FastTrackLocal(int resetBlocksSeen, int activeCommittee) {
    this.resetBlocksSeen = resetBlocksSeen;
    this.activeCommittee = activeCommittee;
  }

  static FastTrackLocal createFromStateAccessor(StateAccessor accessor) {
    return new FastTrackLocal(
        accessor.get("resetBlocksSeen").intValue(), accessor.get("activeCommittee").intValue());
  }

  /**
   * Increases blocks seen and increases active committees if there is and new committee.
   *
   * @param committees number of committees
   * @return new state
   */
  public FastTrackLocal increaseBlocksSeen(int committees) {
    if (committees > activeCommittee) {
      return new FastTrackLocal(resetBlocksSeen + 1, activeCommittee + 1);
    } else {
      return new FastTrackLocal(resetBlocksSeen + 1, activeCommittee);
    }
  }

  /**
   * Get active committee id.
   *
   * @return id of active committee
   */
  public int getActiveCommittee() {
    return activeCommittee;
  }

  /**
   * Get number of reset blocks seen by plugin.
   *
   * @return number of reset blocks seen
   */
  public int getResetBlocksSeen() {
    return resetBlocksSeen;
  }
}
