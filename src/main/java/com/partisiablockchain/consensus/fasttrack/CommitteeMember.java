package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.stream.SafeListStream;

/**
 * A member of a committee (see {@link Committee}) in fast track. Contains the identity and the key
 * used in production.
 */
@Immutable
public final class CommitteeMember implements StateSerializableInline {

  /** Utility that reads a list of {@link CommitteeMember} from a stream. */
  public static final SafeListStream<CommitteeMember> LIST_STREAM =
      SafeListStream.create(
          s -> {
            BlockchainAddress identity = BlockchainAddress.read(s);
            BlockchainPublicKey key = BlockchainPublicKey.read(s);
            BlsPublicKey blsKey = BlsPublicKey.read(s);
            return new CommitteeMember(identity, key, blsKey);
          },
          (member, stream) -> {
            member.getIdentity().write(stream);
            member.getProducerKey().write(stream);
            member.getProducerBlsKey().write(stream);
          });

  private final BlockchainAddress identity;
  private final BlockchainPublicKey producerKey;
  private final BlsPublicKey producerBlsKey;

  static CommitteeMember createFromStateAccessor(StateAccessor accessor) {
    return new CommitteeMember(
        accessor.get("identity").blockchainAddressValue(),
        accessor.get("producerKey").blockchainPublicKeyValue(),
        accessor.get("producerBlsKey").blsPublicKeyValue());
  }

  @SuppressWarnings("unused")
  private CommitteeMember() {
    identity = null;
    producerKey = null;
    producerBlsKey = null;
  }

  /**
   * Create a new committee member.
   *
   * @param identity the address of the member
   * @param producerKey the member's signing key
   * @param producerBlsKey the member's BLS key for finalization
   */
  public CommitteeMember(
      BlockchainAddress identity, BlockchainPublicKey producerKey, BlsPublicKey producerBlsKey) {
    this.identity = identity;
    this.producerKey = producerKey;
    this.producerBlsKey = producerBlsKey;
  }

  /**
   * Get address of member.
   *
   * @return blockchain address
   */
  public BlockchainAddress getIdentity() {
    return identity;
  }

  /**
   * Get signing key of member.
   *
   * @return signing key
   */
  public BlockchainPublicKey getProducerKey() {
    return producerKey;
  }

  /**
   * Get BLS key of member.
   *
   * @return BLS key for finalization
   */
  public BlsPublicKey getProducerBlsKey() {
    return producerBlsKey;
  }
}
