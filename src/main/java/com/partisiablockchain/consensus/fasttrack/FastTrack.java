package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import java.util.List;

/** Fast track consensus on-chain plugin, check that everything is run according to the protocol. */
@Immutable
public final class FastTrack extends BlockchainConsensusPlugin<FastTrackGlobal, FastTrackLocal> {

  @Override
  public BlockValidation validateLocalBlock(
      FastTrackGlobal global, FastTrackLocal local, FinalBlock block) {
    boolean isCorrectCommittee = block.getBlock().getCommitteeId() == local.getActiveCommittee();

    boolean hasPendingCommitteeToActivate =
        local.getActiveCommittee() < global.getNewestCommitteeId();
    boolean isResetBlock = block.getBlock().getProducerIndex() == -1;

    if (isCorrectCommittee) {
      if (hasPendingCommitteeToActivate) {
        if (isResetBlock) {
          return global.validateFinalizedBlock(block);
        } else {
          return BlockValidation.createRejected();
        }
      } else {
        return global.validateFinalizedBlock(block);
      }
    } else {
      return BlockValidation.createRejected();
    }
  }

  @Override
  public boolean validateExternalBlock(FastTrackGlobal global, FinalBlock block) {
    return global.validateFinalizedBlock(block).isAccepted();
  }

  @Override
  public Class<FastTrackGlobal> getGlobalStateClass() {
    return FastTrackGlobal.class;
  }

  @Override
  public InvokeResult<FastTrackGlobal> invokeGlobal(
      PluginContext pluginContext, FastTrackGlobal state, byte[] bytes) {
    return new InvokeResult<>(SafeDataInputStream.readFully(bytes, state::invoke), null);
  }

  @Override
  public FastTrackLocal updateForBlock(
      PluginContext pluginContext, FastTrackGlobal global, FastTrackLocal local, Block block) {
    if (block.getProducerIndex() == -1) {
      return local.increaseBlocksSeen(global.getNewestCommitteeId());
    } else {
      return local;
    }
  }

  @Override
  public FastTrackLocal migrateLocal(StateAccessor stateAccessor) {
    if (stateAccessor == null) {
      return new FastTrackLocal();
    } else {
      return FastTrackLocal.createFromStateAccessor(stateAccessor);
    }
  }

  @Override
  public FastTrackGlobal migrateGlobal(StateAccessor stateAccessor, SafeDataInputStream rpc) {
    if (stateAccessor == null) {
      List<CommitteeMember> producers = CommitteeMember.LIST_STREAM.readDynamic(rpc);
      Hash initialId = Hash.read(rpc);
      return FastTrackGlobal.initialize(initialId, producers);
    } else {
      return FastTrackGlobal.createFromStateAccessor(stateAccessor);
    }
  }

  @Override
  public Class<FastTrackLocal> getLocalStateClass() {
    return FastTrackLocal.class;
  }
}
