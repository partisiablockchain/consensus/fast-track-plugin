package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin.BlockValidation;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.bls.BlsVerifier;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** The global state of the fast track plugin. */
@Immutable
public final class FastTrackGlobal implements StateSerializable {

  private static final Logger logger = LoggerFactory.getLogger(FastTrackGlobal.class);

  private final FixedList<Committee> committees;

  /** Construct uninitialized FastTrackGlobal. */
  @SuppressWarnings("unused")
  public FastTrackGlobal() {
    committees = null;
  }

  private FastTrackGlobal(FixedList<Committee> committees) {
    this.committees = committees;
  }

  static FastTrackGlobal createFromStateAccessor(StateAccessor stateAccessor) {
    FixedList<Committee> committees =
        FixedList.create(
            stateAccessor.get("committees").getListElements().stream()
                .map(Committee::createFromStateAccessor));
    return new FastTrackGlobal(committees);
  }

  private static Committee verifyCommittee(Hash committeeId, FixedList<CommitteeMember> producers) {
    if (!validSize(producers)) {
      throw new IllegalArgumentException("Illegal committee size. Must be of size 3t+1.");
    } else if (hasDuplicates(producers)) {
      throw new IllegalArgumentException("Producers contain duplicate addresses");
    } else {
      return new Committee(committeeId, producers);
    }
  }

  private static boolean validSize(Collection<CommitteeMember> producers) {
    return producers.size() > 3;
  }

  static FastTrackGlobal initialize(Hash initialCommitteeId, List<CommitteeMember> producers) {
    FixedList<CommitteeMember> allProducers = FixedList.create(producers);
    Committee committee = verifyCommittee(initialCommitteeId, allProducers);
    return new FastTrackGlobal(FixedList.create(List.of(committee)));
  }

  private static boolean hasDuplicates(Collection<CommitteeMember> producers) {
    HashSet<BlockchainAddress> uniqueProducers = new HashSet<>();
    HashSet<BlockchainPublicKey> uniqueKeys = new HashSet<>();
    for (CommitteeMember producer : producers) {
      uniqueProducers.add(producer.getIdentity());
      uniqueKeys.add(producer.getProducerKey());
    }
    return uniqueProducers.size() != producers.size() || uniqueKeys.size() != producers.size();
  }

  /**
   * Get the production keys for the given committee.
   *
   * @param committeeId the ID of the committee to get the keys from
   * @return the public keys for the given committee
   */
  public List<BlockchainPublicKey> getCommitteeKeys(long committeeId) {
    return committees.get((int) committeeId).getActiveCommittee().stream()
        .map(CommitteeMember::getProducerKey)
        .collect(Collectors.toList());
  }

  /**
   * Get the BLS keys of the given committee. These keys are used during block finalization.
   *
   * @param committeeId the ID of the committee
   * @return a list of BLS public keys of the committee.
   */
  public List<BlsPublicKey> getCommitteeBlsKeys(long committeeId) {
    return committees.get((int) committeeId).getActiveCommittee().stream()
        .map(CommitteeMember::getProducerBlsKey)
        .collect(Collectors.toList());
  }

  /**
   * Get identifying hash of committee.
   *
   * @param committeeId the ID of the committee
   * @return identifying hash
   */
  public Hash getCommitteeId(long committeeId) {
    return committees.get((int) committeeId).getCommitteeId();
  }

  /**
   * Returns the index of the newest committee.
   *
   * @return index of newest committee
   */
  public int getNewestCommitteeId() {
    return committees.size() - 1;
  }

  BlockValidation validateFinalizedBlock(FinalBlock finalizedBlock) {
    if (committees == null) {
      throw new IllegalArgumentException("FastTrackGlobal not initialized");
    }
    if (finalizedBlock.getBlock().getCommitteeId() > getNewestCommitteeId()) {
      logger.info("No committee exists for the given committee ID, rejecting the block");
      return BlockValidation.createRejected();
    }
    byte[] finalizationData = finalizedBlock.getFinalizationData();
    ByteArrayInputStream bytes = new ByteArrayInputStream(finalizationData);
    SafeDataInputStream stream = new SafeDataInputStream(bytes);
    BlockValidation result = verifyFinalBlock(finalizedBlock, stream);
    if (bytes.available() != 0) {
      logger.info("Rejecting block since it contains unused finalization data");
      return BlockValidation.createRejected();
    }
    return result;
  }

  private BlockValidation verifyFinalBlock(FinalBlock finalizedBlock, SafeDataInputStream stream) {
    Hash identifier = finalizedBlock.getBlock().identifier();
    int committeeId = (int) finalizedBlock.getBlock().getCommitteeId();
    if (verifySignatures(identifier, stream, committeeId)) {
      short producerIndex = finalizedBlock.getBlock().getProducerIndex();
      return BlockValidation.createAccepted(producerIndex != -1);
    } else {
      return BlockValidation.createRejected();
    }
  }

  boolean verifySignatures(
      Hash identifier, SafeDataInputStream finalizationDataStream, int committeeId) {
    List<BlsPublicKey> committeeBlsKeys = getCommitteeBlsKeys(committeeId);
    int maliciousCount = (committeeBlsKeys.size() - 1) / 3;
    int neededSignatures = committeeBlsKeys.size() - maliciousCount;
    return verifySignatures(finalizationDataStream, identifier, neededSignatures, committeeBlsKeys);
  }

  private boolean verifySignatures(
      SafeDataInputStream finalizationDataStream,
      Hash identifier,
      int signaturesNeeded,
      List<BlsPublicKey> committeeKeys) {

    byte[] bitVector = finalizationDataStream.readDynamicBytes();
    final BlsSignature sig = BlsSignature.read(finalizationDataStream);

    List<Integer> signers = getSet(bitVector, committeeKeys.size());
    if (signers.size() < signaturesNeeded) {
      logger.info(
          "Rejecting due to insufficient number of contributors. Got {}, but expected {}",
          signers.size(),
          signaturesNeeded);
      return false;
    }

    // we're guaranteed to aggregate some key consisting of at least signaturesNeeded keys here, so
    // correctness of the signature is the only thing we need to check.
    BlsPublicKey key = computeAggregatedKey(committeeKeys, signers);
    boolean signatureValid = BlsVerifier.verify(identifier, sig, key);
    if (!signatureValid) {
      logger.info("Rejecting due to invalid signature");
      return false;
    }

    return true;
  }

  /**
   * Get activated bits among the first m bits of the input.
   *
   * @param bitVector a bit vector.
   * @param m an integer s.t. m / 8 <= bitVector.length.
   * @return a list of indices L s.t. bitVector[L[i]] == 1.
   */
  static List<Integer> getSet(byte[] bitVector, int m) {
    int bits = m / 8;
    int leftover = m % 8;
    int i = 0;
    List<Integer> activated = new ArrayList<>();
    for (; i < bits; i++) {
      byte c = bitVector[i];
      for (int j = 0; j < 8; j++) {
        if (((c >> j) & 1) == 1) {
          activated.add(i * 8 + j);
        }
      }
    }
    for (int j = 0; j < leftover; j++) {
      if (((bitVector[i] >> j) & 1) == 1) {
        activated.add(i * 8 + j);
      }
    }
    return activated;
  }

  private static BlsPublicKey computeAggregatedKey(
      List<BlsPublicKey> committeeBlsKeys, List<Integer> signerIndices) {
    BlsPublicKey key = committeeBlsKeys.get(signerIndices.get(0));
    for (int i = 1; i < signerIndices.size(); i++) {
      key = key.addPublicKey(committeeBlsKeys.get(signerIndices.get(i)));
    }
    return key;
  }

  /**
   * Invoke a command on fast track plugin.
   *
   * @param rpc invocation
   * @return state of fast track plugin
   */
  public FastTrackGlobal invoke(SafeDataInputStream rpc) {
    return rpc.readEnum(GlobalInvocation.values()).invoke(this, rpc);
  }

  /**
   * Set the current committee from a list of producers.
   *
   * @param committeeId id of committee
   * @param producers list of producers
   * @return updated state
   */
  public FastTrackGlobal setCommittee(Hash committeeId, FixedList<CommitteeMember> producers) {
    Committee committee = verifyCommittee(committeeId, producers);
    return new FastTrackGlobal(committees.addElement(committee));
  }
}
