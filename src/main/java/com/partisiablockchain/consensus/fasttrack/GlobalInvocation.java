package com.partisiablockchain.consensus.fasttrack;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** Invocations for the global fast track state. */
public enum GlobalInvocation {
  /** Sets current committee from a list of producers. */
  SET_COMMITTEE {
    @Override
    FastTrackGlobal invoke(FastTrackGlobal global, SafeDataInputStream rpc) {
      Hash committeeId = Hash.read(rpc);
      List<CommitteeMember> producers = CommitteeMember.LIST_STREAM.readDynamic(rpc);
      return global.setCommittee(committeeId, FixedList.create(producers));
    }
  };

  abstract FastTrackGlobal invoke(FastTrackGlobal global, SafeDataInputStream rpc);
}
